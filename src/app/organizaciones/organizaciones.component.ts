import { Component, OnInit } from '@angular/core';
import { Organizacion } from './organizacion';
import { OrganizacionService } from './organizacion.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-organizaciones',
  templateUrl: './organizaciones.component.html'
})
export class OrganizacionesComponent implements OnInit {

  organizaciones: Organizacion[];
  nombre: string;
  direccion: string;
  telefono: string;
  idExterno: string;
  codigoEncriptacion: string;

  constructor(private organizacionService: OrganizacionService) {}

  ngOnInit() {
    this.organizacionService.getOrganizaciones().subscribe(
      organizaciones => this.organizaciones = organizaciones
    );
  }

  delete(organizacion: Organizacion): void {
    swal({
      title: '¿Està seguro?',
      text: `¿Seguro que desea eliminar a la organizacion ${organizacion.nombre}?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar',
      cancelButtonText: 'No, cancelar',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }). then((result) => {
      if (result.value) {
        this.organizacionService.delete(organizacion.id).subscribe(
          response => {
            this.organizaciones = this.organizaciones.filter(org => org !== organizacion);
            swal(
              'Organización Elimnada',
              `Organización ${organizacion.nombre} eliminada con éxito`,
              'success'
            )
          }
        )
      }
    })
  }

  validar(){
    document.getElementById('cerrarModal').click();
    const inputElement: HTMLInputElement = document.getElementById("codigoEncriptacionUsuario") as HTMLInputElement;
    const inputValue: string = inputElement.value;
    inputElement.value='';
    if (inputValue == this.codigoEncriptacion) {
        document.getElementById('mostrarModalDatos').click();
        const modalDatos = document.getElementById("modalDatos");
        modalDatos.classList.add('show');
        modalDatos.setAttribute('aria-modal', 'true');
        modalDatos.setAttribute('style', 'display:block')
    } else {
      swal(
        'Código no válido',
        'Los datos introducidos no son correctos',
        'error'
      )
    }
  }

  ver(organizacion: Organizacion) {
    this.nombre = organizacion.nombre;
    this.direccion = organizacion.direccion;
    this.telefono = organizacion.telefono;
    this.idExterno = organizacion.idExterno;
    this.codigoEncriptacion = organizacion.codigoEncriptacion;
  }

}
