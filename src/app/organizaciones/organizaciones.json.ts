import { Organizacion } from './organizacion';

export const ORGANIZACIONES: Organizacion[] = [
  {id: 1, nombre: 'Organizacion Prueba 1', direccion: 'Culhuacan 2', telefono: '5521273461', idExterno: 'ORGA3461NE001', codigoEncriptacion: '1234'},
  {id: 2, nombre: 'Organizacion Prueba 2', direccion: 'Culhuacan 2', telefono: '5521273461', idExterno: 'ORGA3461NE002', codigoEncriptacion: '1234'},
  {id: 3, nombre: 'Organizacion Prueba 3', direccion: 'Culhuacan 2', telefono: '5521273461', idExterno: 'ORGA3461NE003', codigoEncriptacion: '1234'}
];
