export class Organizacion {
  id: number;
  nombre: string;
  direccion: string;
  telefono: string;
  idExterno: string;
  codigoEncriptacion: string;
}
