import { Injectable } from '@angular/core';
import { Organizacion } from './organizacion';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import swal from 'sweetalert2';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class OrganizacionService {

  private urlEndpoint: string = 'http://localhost:8080/api/organizaciones';
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http: HttpClient, private router: Router) { }

  getOrganizaciones(): Observable<Organizacion[]> {
    return this.http.get(this.urlEndpoint).pipe(
      map(response => response as Organizacion[])
    );
  }

  create(organizacion: Organizacion) : Observable<any> {
    return this.http.post<Organizacion>(this.urlEndpoint, organizacion, {headers: this.httpHeaders}).pipe(
      catchError(e => {
        console.error(e.error.mensaje);
        swal(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
    );
  }

  getOrganizacion(id: number): Observable<Organizacion>{
    return this.http.get<Organizacion>(`${this.urlEndpoint}/${id}`).pipe(
      catchError(e => {
        this.router.navigate(['/organizaciones']);
        console.error(e.error.mensaje);
        swal('Error al editar', e.error.mensaje, 'error');
        return throwError(e);
      })
    );
  }

  update(organizacion: Organizacion): Observable<any> {
    return this.http.put<any>(`${this.urlEndpoint}/${organizacion.id}`, organizacion, {headers: this.httpHeaders}).pipe(
      catchError(e => {
        console.error(e.error.mensaje);
        swal(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
    );
  }

  delete(id: number): Observable<Organizacion> {
    return this.http.delete<Organizacion>(`${this.urlEndpoint}/${id}`, {headers: this.httpHeaders}).pipe(
      catchError(e => {
        console.error(e.error.mensaje);
        swal(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
    );
  }
}
