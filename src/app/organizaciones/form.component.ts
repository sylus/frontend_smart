import { Component, OnInit } from '@angular/core';
import { Organizacion } from './organizacion';
import { OrganizacionService } from './organizacion.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

  private organizacion: Organizacion = new Organizacion();
  private titulo: string = "Crear Organización";

  constructor(
    private organizacionService: OrganizacionService, private router: Router, private activateRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.cargarOrganizacion();
  }

  cargarOrganizacion() {
    this.activateRoute.params.subscribe(params => {
      let id = params['id'];
      if(id) {
        this.organizacionService.getOrganizacion(id).subscribe((organizacion) => this.organizacion = organizacion);
      }
    })
  }

  create(): void {
    this.organizacionService.create(this.organizacion).subscribe(
      json => {
        this.router.navigate(['/organizaciones']);
        swal('Organización guardada', `Organizacion ${json.organizacion.nombre} creado con éxtio`, 'success');
      }
    )
  }

  update(): void {
    this.organizacionService.update(this.organizacion).subscribe(json => {
      this.router.navigate(['/organizaciones']);
      swal('Organización Actualizado', `Organización ${json.organizacion.nombre} actualizado con éxito`, 'success');
    })
  }

}
